<?php


namespace App;


class PopularWords
{

    /**
     * @var array
     */
    private $words = [];

    /**
     * @var array
     */
    private $ignoreWords = [];

    /**
     * @param $text
     */
    public function addText(string $text): void
    {
        $array = array_count_values(str_word_count($text, 1, '1234567890'));
        $array = array_map(function($string){
            return trim($string, '\'-');
        }, $array);
        $array = array_filter($array,'is_string', ARRAY_FILTER_USE_KEY);
        foreach ($array as $word=>$count)
        {
            $this->words[$word] = $this->words[$word] ?? 0;
            $this->words[$word] += (int)$count;
        }
    }

    /**
     * @param $words
     */
    public function addIgnoreWords(array $words): void
    {
        $this->ignoreWords = array_merge($this->ignoreWords, $words);
    }

    /**
     * @param int $limit
     * @return array
     */
    public function getTop(int $limit = 10): array
    {
        arsort($this->words);
        $words = array_diff_key($this->words, array_combine($this->ignoreWords, $this->ignoreWords));
        return array_slice($words, 0, $limit);
    }
}