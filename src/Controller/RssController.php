<?php

namespace App\Controller;

use App\Reader;
use FeedIo\FeedIo;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class RssController extends AbstractController
{
    /**
     * @Route("/rss", name="rss")
     */
    public function index(Reader $reader)
    {
        $result = $reader->read();
        return $this->render('rss/index.html.twig', [
            'topWorlds' => $result->getTop(),
            'feed' => $result->getFeed()
        ]);
    }
}

