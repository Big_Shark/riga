<?php

namespace App;

use FeedIo\FeedIo;

class Reader
{
    /**
     * @var FeedIo
     */
    private $feedIo;
    /**
     * @var PopularWords
     */
    private $popularWords;

    /**
     * Reader constructor.
     * @param FeedIo $feedIo
     * @param PopularWords $popularWords
     */
    public function __construct(FeedIo $feedIo, PopularWords $popularWords, TopWords $topWords)
    {
        $this->feedIo = $feedIo;
        $this->popularWords = $popularWords;
        $this->popularWords->addIgnoreWords($topWords->getWords(50));
    }

    /**
     * @return Result
     */
    public function read()
    {
        $feed = $this->feedIo->read('https://www.theregister.co.uk/software/headlines.atom')->getFeed();
        foreach ($feed as $element)
        {
            $this->popularWords->addText($element->getTitle());
        }

        return new Result($feed, $this->popularWords->getTop(10));
    }}