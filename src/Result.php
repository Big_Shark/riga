<?php


namespace App;


use FeedIo\FeedInterface;

class Result
{
    /**
     * @var FeedInterface
     */
    private $feed;
    /**
     * @var array
     */
    private $top;

    /**
     * Result constructor.
     * @param FeedInterface $feed
     * @param array $top
     */
    public function __construct(FeedInterface $feed, array $top)
    {

        $this->feed = $feed;
        $this->top = $top;
    }

    /**
     * @return array
     */
    public function getTop(): array
    {

        return $this->top;
    }

    /**
     * @return FeedInterface
     */
    public function getFeed(): FeedInterface
    {

        return $this->feed;
    }
}