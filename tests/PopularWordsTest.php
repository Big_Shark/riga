<?php

namespace App\Tests;

use App\PopularWords;
use PHPUnit\Framework\TestCase;

class PopularWordsTest extends TestCase
{
    public function testGetTop()
    {
        $class = new PopularWords();
        $class->addText('one two three');
        $this->assertSame(['one' => 1, 'two' => 1, 'three' => 1], $class->getTop());

        $class->addText('one two three');
        $this->assertSame(['one' => 2, 'two' => 2, 'three' => 2], $class->getTop());


        $class->addText('one');
        $this->assertSame(['one' => 3, 'two' => 2, 'three' => 2], $class->getTop());

        $class->addText(' one');
        $this->assertSame(['one' => 4, 'two' => 2, 'three' => 2], $class->getTop());

        $class->addText('one ');
        $this->assertSame(['one' => 5, 'two' => 2, 'three' => 2], $class->getTop());

        $class->addIgnoreWords(['two']);
        $this->assertSame(['one' => 5, 'three' => 2], $class->getTop());

        $this->assertSame(['one' => 5], $class->getTop(1));
    }
}
